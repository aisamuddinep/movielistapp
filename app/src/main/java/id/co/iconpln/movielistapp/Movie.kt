package id.co.iconpln.movielistapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie (
    var title: String = "",
    var desc: String = "",
    var genre: String = "",
    var photo: String = "",
    var imbd_url: String = ""
) : Parcelable