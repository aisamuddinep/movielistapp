package id.co.iconpln.movielistapp

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_movie.*
import java.io.File
import java.io.FileOutputStream


class DetailMovieActivity : AppCompatActivity(), View.OnClickListener {

    private val tvDetailTitle: TextView
        get() = tv_detail_title
    private val tvDetailDesc: TextView
        get() = tv_detail_desc
    private val tvDetailGenre: TextView
        get() = tv_detail_genre
    private val ivDetailPhoto: ImageView
        get() = iv_detail_photo
    private val btnDetailShare: Button
        get() = btn_detail_share

    companion object{
        const val EXTRA_MOVIE = "extra_movie"
    }

    private lateinit var movie: Movie
    private val title = "Detail Movie"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        setupActionBar()
        getIntentExtra()
        displayMovieDetail()
        setOnClickButton()
    }

    private fun getIntentExtra() {
        movie = intent.getParcelableExtra(EXTRA_MOVIE) ?: Movie()
    }

    private fun setupActionBar() {
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setOnClickButton() {
        btn_detail_share.setOnClickListener(this)
    }

    private fun displayMovieDetail() {
        tvDetailTitle.text = movie.title
        tvDetailDesc.text = movie.desc
        tvDetailGenre.text = movie.genre

        Glide.with(this)
            .load(movie.photo)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_image_black_24dp)
                    .error(R.drawable.ic_error_outline_black_24dp)
            )
            .into(ivDetailPhoto)

        tvDetailGenre.setTextColor(
            ContextCompat.getColor(
                this,
                ListViewMovieAdapter.GENRE_COLORS[movie.genre] ?: R.color.colorPrimary
            )
        )
    }

    override fun onClick(view: View) {
        when(view.id){
            btnDetailShare.id -> {
                openWeb()
            }
        }
    }

    private fun openWeb() {
        val webPage = Uri.parse(movie.imbd_url)
        val openWebIntent = Intent(Intent.ACTION_VIEW, webPage)
        if (openWebIntent.resolveActivity(packageManager) != null) {
            startActivity(openWebIntent)
        }
    }

    private fun shareMovie() {

        val bitmap: Bitmap = getBitmapFromView(ivDetailPhoto)
        try {
            val file = File(this.externalCacheDir, "movie.png")
            val fOut = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut.flush()
            fOut.close()
            file.setReadable(true, false)
            val shareMovieIntent = Intent(Intent.ACTION_SEND)
            val sharedText = "Movie Title : ${movie.title},\nMovie Description: ${movie.desc}"
            shareMovieIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            shareMovieIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
            shareMovieIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))
            shareMovieIntent.type = "image/png"
            val shareIntent = Intent.createChooser(shareMovieIntent, "Share Movie")
            if (shareIntent.resolveActivity(packageManager) != null) startActivity(shareIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun getBitmapFromView(view: View): Bitmap {
        val returnedBitmap =
            Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) {
            bgDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.WHITE)
        }
        view.draw(canvas)
        return returnedBitmap
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.share_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            R.id.share_movie -> {
                shareMovie()
                true
            }
            else -> {
                false
            }
        }
    }
}
