package id.co.iconpln.movielistapp

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide

class ListViewMovieAdapter(val context: Context, val listMovie: ArrayList<Movie>) : BaseAdapter() {
    @SuppressLint("ViewHolder")
    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewLayout =
            LayoutInflater.from(context).inflate(R.layout.item_list_movie, viewGroup, false)
        val viewHolder = ViewHolder(viewLayout)
        val movie = getItem(index) as Movie
        viewHolder.bind(context, movie)

        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listMovie[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listMovie.size
    }

    private inner class ViewHolder(view: View) {
        private val tvMovieTitle: TextView = view.findViewById(R.id.tv_movie_title)
        private val tvMovieDesc: TextView = view.findViewById(R.id.tv_movie_desc)
        private val tvMovieGenre: TextView = view.findViewById(R.id.tv_movie_genre)
        private val ivMoviePhoto: ImageView = view.findViewById(R.id.iv_movie_photo)

        private val titleTypeFace = ResourcesCompat.getFont(context, R.font.chunk_five_regular)
        private val descTypeFace = ResourcesCompat.getFont(context, R.font.oswald_light)

        fun bind(context: Context, movie: Movie) {
            tvMovieTitle.text = movie.title
            tvMovieDesc.text = movie.desc
            tvMovieGenre.text = movie.genre

            Glide.with(context)
                .load(movie.photo)
                .into(ivMoviePhoto)

            tvMovieTitle.typeface = titleTypeFace
            tvMovieDesc.typeface = descTypeFace

            tvMovieGenre.setTextColor(
                ContextCompat.getColor(
                    context,
                    GENRE_COLORS[movie.genre] ?: R.color.colorPrimary
                )
            )
        }

    }

    companion object {
        val GENRE_COLORS = hashMapOf(
            "Action" to android.R.color.holo_red_light,
            "Animation" to android.R.color.holo_blue_dark
        )
    }
}