package id.co.iconpln.movielistapp

import android.content.Intent
import android.os.Bundle
import android.util.Patterns.EMAIL_ADDRESS
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private val etLoginEmail: EditText
        get() = et_login_email

    private val etLoginPassword: EditText
        get() = et_login_password

    private val btnLogin: Button
        get() = btn_login_submit

    private var email = ""
    private var password = ""
    private var minPassword = 7
    private val msgMinVal = " Must be More or Equal to "
    private val msgWrongFormat = " Wrong Format"
    private val cannotBlank = " Can't be Blank"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setOnClickListener()
    }

    private fun setOnClickListener() {
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view.id == btnLogin.id) {
            getInput()
            if (loginValidation()) {
                val mainIntent = Intent(this, MainActivity::class.java)
                startActivity(mainIntent)
                finish()
            }
        }
    }

    private fun getInput() {
        email = etLoginEmail.text.toString()
        password = etLoginPassword.text.toString()
    }

    private fun loginValidation(): Boolean {
        return if (checkField()) {
            if (email == "user@mail.com" && password == "password") true
            else {
                val alertBuilder = AlertDialog.Builder(this)
                alertBuilder.setTitle(getString(R.string.title_login_failed))
                alertBuilder.setMessage(getString(R.string.msg_login_failed))
                alertBuilder.setPositiveButton(getString(R.string.try_again)) { _, _ -> }
                alertBuilder.show()

                false
            }
        } else false
    }

    private fun checkField(): Boolean {
        val validEmail = checkBlankField(etLoginEmail) && validateEmail(etLoginEmail)
        val validPassword = checkBlankField(etLoginPassword) && checkMinValue(minPassword, etLoginPassword)
        return validEmail && validPassword
    }

    private fun checkMinValue(limit: Int, editText: EditText): Boolean {
        return if (editText.text.length < limit) {
            editText.error = editText.hint.toString() + msgMinVal + limit.toString()
            false
        } else true
    }

    private fun validateEmail(editText: EditText): Boolean {
        return if (EMAIL_ADDRESS.matcher(editText.text.toString()).matches()) {
            true
        } else {
            editText.error = editText.hint.toString() + msgWrongFormat
            false
        }
    }

    private fun checkBlankField(editText: EditText): Boolean {
        return if (editText.text.isBlank()) {
            editText.error = editText.hint.toString() + cannotBlank
            false
        } else true
    }

}
