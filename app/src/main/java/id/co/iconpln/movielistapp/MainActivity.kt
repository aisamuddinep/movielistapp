package id.co.iconpln.movielistapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val listMovie: ListView
        get() = lv_list_movie

    private var list: ArrayList<Movie> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadListBaseAdapter(this)
        setListClickListener(listMovie)
    }

    private fun setListClickListener(listView: ListView) {
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, _, i, _ ->
                val movie = adapterView?.getItemAtPosition(i) as Movie
                Toast.makeText(this@MainActivity, "${i+1}. ${movie.title}", Toast.LENGTH_SHORT).show()
                showDetailMovie(movie)
            }
    }

    private fun showDetailMovie(movie: Movie) {
        val detailMovieIntent = Intent(this,DetailMovieActivity::class.java)
        detailMovieIntent.putExtra(DetailMovieActivity.EXTRA_MOVIE, movie)
        startActivity(detailMovieIntent)
    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(MoviesData.listDataMovie)

        val baseAdapter = ListViewMovieAdapter(context, list)
        listMovie.adapter = baseAdapter
    }
}
