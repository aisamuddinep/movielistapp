package id.co.iconpln.movielistapp

object MoviesData {
    val listDataMovie: ArrayList<Movie>
        get() {
            // Create Empty List Movie
            val list = ArrayList<Movie>()
            for (data in dataMovies){
                val movie = Movie()
                movie.title = data[0]
                movie.desc = data[1]
                movie.genre= data[2]
                movie.photo = data[3]
                movie.imbd_url = data[4]
                list.add(movie)
            }
            return list
        }

    private var dataMovies = arrayOf(
        arrayOf(
            "Avengers: Endgame",
            "After Thanos, an intergalactic warlord, disintegrates half of the universe, the Avengers must reunite and assemble again to reinvigorate their trounced allies and restore balance.",
            "Action",
            "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_.jpg",
            "https://www.imdb.com/title/tt4154796/"
        ),
        arrayOf(
            "Star Wars: The Force Awakens",
            "A new order threatens to destroy the New Republic. Finn, Rey and Poe, backed by the Resistance and the Republic, must find a way to stop them and find Luke, the last surviving Jedi.",
            "Action",
            "https://images-na.ssl-images-amazon.com/images/I/71rZtELyYzL._SY679_.jpg",
            "https://www.imdb.com/title/tt2488496/"
        ),
        arrayOf(
            "The Avengers",
            "Nick Fury is compelled to launch the Avengers Initiative when Loki poses a threat to planet Earth. His squad of superheroes put their minds together to accomplish the task.",
            "Action",
            "https://images-na.ssl-images-amazon.com/images/I/719SFBdxRtL._SY550_.jpg",
            "https://www.imdb.com/title/tt0848228/"
        ),
        arrayOf(
            "Incredibles 2",
            "Entrusted with a task to restore the public's faith in the superheroes, Helen sets off on a mission to catch a supervillain while Bob faces the challenges of a stay-at-home parenting.",
            "Animation",
            "https://images-na.ssl-images-amazon.com/images/I/71a5g3Lop4L._SY679_.jpg",
            "https://www.imdb.com/title/tt3606756/"
        ),
        arrayOf(
            "Aquaman",
            "Half-human, half-Atlantean Arthur is born with the ability to communicate with marine creatures. His motive is to retrieve the legendary Trident of Atlan and protect the water world.",
            "Action",
            "https://images-na.ssl-images-amazon.com/images/I/71JGOXv98RL._SY606_.jpg",
            "https://www.imdb.com/title/tt4154796/"
        ),
        arrayOf(
            "Venom",
            "While trying to take down Carlton, the CEO of Life Foundation, Eddie, a journalist, investigates experiments of human trials. Unwittingly, he gets merged with a symbiotic alien with lethal abilities.",
            "Action",
            "https://images-na.ssl-images-amazon.com/images/I/9187a8t2caL._SY741_.jpg",
            "https://www.imdb.com/title/tt1270797/"
        ),
        arrayOf(
            "Captain Marvel",
            "Captain Marvel is an extraterrestrial Kree warrior who finds herself caught in the middle of an intergalactic battle between her people and the Skrulls. Living on Earth in 1995, she keeps having recurring memories of another life as U.S. Air Force pilot Carol Danvers. With help from Nick Fury, Captain Marvel tries to uncover the secrets of her past while harnessing her special superpowers to end the war with the evil Skrulls.",
            "Action",
            "https://images-na.ssl-images-amazon.com/images/I/71K3S%2BTk4OL._SY679_.jpg",
            "https://www.imdb.com/title/tt4154664/"
        ),
        arrayOf(
            "Toy Story 3",
            "Andy's toys get mistakenly delivered to a day care centre. Woody convinces the other toys that they weren't dumped and leads them on an expedition back home.",
            "Animation",
            "https://contentserver.com.au/assets/648681_toy_story_3_v8.jpg",
            "https://www.imdb.com/title/tt0435761/"
        ),
        arrayOf(
            "Gemini Man",
            "Henry Brogan is an elite 51-year-old assassin who's ready to call it quits after completing his 72nd job. His plans get turned upside down when he becomes the target of a mysterious operative who can seemingly predict his every move. To his horror, Brogan soon learns that the man who's trying to kill him is a younger, faster, cloned version of himself.",
            "Action",
            "https://images-na.ssl-images-amazon.com/images/I/71QEq--8H-L._SY550_.jpg",
            "https://www.imdb.com/title/tt1025100/"
        ),
        arrayOf(
            "Iron Man 3",
            "Tony Stark encounters a formidable foe called the Mandarin. After failing to defeat his enemy, he embarks on a journey with relentless consequences and a suit that ceases to exist.",
            "Action",
            "https://images-na.ssl-images-amazon.com/images/I/51WoeTa7ZHL.jpg",
            "https://www.imdb.com/title/tt1300854/"
        ),
        arrayOf(
            "Fast And Furious 8",
            "Dom encounters a mysterious woman, Cipher, who gets him involved in the world of terrorism. The crew has to reunite to stop Cipher and save the man who brought them together as a family.",
            "Action",
            "https://i.pinimg.com/474x/84/7f/72/847f72b5f3fe337e8637f456b73d92ac--usa-tv-new-movie-posters.jpg",
            "https://www.imdb.com/title/tt4630562/"
        )

    )
}